# concordance
DESCRIPTION: Given an arbitrary text document written in English, write a program that will generate a
concordance, i.e. an alphabetical list of all word occurrences, labeled with word frequencies.
Bonus: label each word with the sentence numbers in which each occurrence appeared.
## Installation

Install 'Leiningen'. http://leiningen.org/

## Usage

Execute 'lein run {ENTER-TEXT-HERE}'

## Examples
lein run 'Given an arbitrary text document written in English, write a program that will generate a
concordance, i.e. an alphabetical list of all word occurrences, labeled with word frequencies.
Bonus: label each word with the sentence numbers in which each occurrence appeared.'

## Example Result:
{:word a, :freq 2, :sentence-indexes (1)}
{:word all, :freq 1, :sentence-indexes (1)}
{:word alphabetical, :freq 1, :sentence-indexes (1)}
{:word an, :freq 2, :sentence-indexes (1)}
{:word appeared, :freq 1, :sentence-indexes (2)}
{:word arbitrary, :freq 1, :sentence-indexes (1)}
{:word bonus, :freq 1, :sentence-indexes (2)}
{:word concordance, :freq 1, :sentence-indexes ()}
{:word document, :freq 1, :sentence-indexes (1)}
{:word each, :freq 2, :sentence-indexes (2)}
{:word english, :freq 1, :sentence-indexes (1)}
{:word frequencies, :freq 1, :sentence-indexes (1)}
{:word generate, :freq 1, :sentence-indexes (1)}
{:word given, :freq 1, :sentence-indexes (1)}
{:word ie, :freq 1, :sentence-indexes ()}
{:word in, :freq 2, :sentence-indexes (1 2)}
{:word label, :freq 1, :sentence-indexes (2)}
{:word labeled, :freq 1, :sentence-indexes (1)}
{:word list, :freq 1, :sentence-indexes (1)}
{:word numbers, :freq 1, :sentence-indexes (2)}
{:word occurrence, :freq 1, :sentence-indexes (2)}
{:word occurrences, :freq 1, :sentence-indexes (1)}
{:word of, :freq 1, :sentence-indexes (1)}
{:word program, :freq 1, :sentence-indexes (1)}
{:word sentence, :freq 1, :sentence-indexes (2)}
{:word text, :freq 1, :sentence-indexes (1)}
{:word that, :freq 1, :sentence-indexes (1)}
{:word the, :freq 1, :sentence-indexes (2)}
{:word which, :freq 1, :sentence-indexes (2)}
{:word will, :freq 1, :sentence-indexes (1)}
{:word with, :freq 2, :sentence-indexes (1 2)}
{:word word, :freq 3, :sentence-indexes (1 2)}
{:word write, :freq 1, :sentence-indexes (1)}
{:word written, :freq 1, :sentence-indexes (1)}

### Deficiencies
-The 'get-words' function will split a string by the space character into a list. It then removes specific characters from the string i.e ':'.
The problem here is the developer has to hardcode characters to remove from the word string. The solution is to improve this function by creating a function that removes any none alphabetic characters except " ' ".

-The 'get-sentences' function will split a string by the period character into a list. The problem here is that this will incorrectly count 'e' as a sentence because of the word 'i.e'. Got around this by replace the 'i.e.' with 'ie'.
This is still a bandaid because the developer will have to account for 'everything'. 
To improve this function, the additional rules can be added:
a) Verify against a list of word abbreviations.
b) Verifying the next character in the string, ie looking for capitalization.
c) Create and verify a list of sentence 'teminators'.

## License

Copyright © 2016 FIXME

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
