(ns concordance.core
  (:use [clojure.string :as str])
  (:gen-class))

;;splits a string with reg-ex
(defn split-string [chr in-str]
 (str/split in-str chr))

;;applies split-string to a string or a list of strings
(defn string-breakdown [reg-ex in-data]
  (cond
    (= (type in-data) java.lang.String) (into '() (split-string reg-ex in-data))
    (= (type in-data) clojure.lang.PersistentList) (into '() (flatten (map #(split-string reg-ex %) in-data)))))

;;function to determine what 'words' are from a given string. Returns a map
(defn get-words [in-str]
  (->> (map #(str/replace % ":" "") (string-breakdown #"\s" in-str))
       (map #(str/replace % "." ""))
       (map #(str/replace % "," ""))
       (remove empty?)
       (map #(lower-case %))))

;;utility boolean function that returns true is an item is found in a coll
(defn in?
  [coll elm]
  (some #(= elm %) coll))

;;function to determine what 'sentences' are from a given string. Returns a map
(defn get-sentences [in-str]
  (-> (str/replace in-str "i.e." "ie")
      (str/replace #"\n" "")
      (str/split  #"\." )))

;;returns the index of the sentence the word is found in
(defn get-sentence-index [in-str in-sentence in-sentence-list]
  (if (= true (in? (get-words in-sentence) (lower-case in-str))) (+ 1 (.indexOf in-sentence-list in-sentence))))

;;applies 'get-sentence-index' to sentence lsit
(defn get-word-indexes [in-str in-sentences]
  (remove nil? (map #(get-sentence-index in-str % in-sentences) in-sentences)))

;;builds a map that contains the result report
(defn create-word-map [in-word-map in-sentences]
  (let [cay (first in-word-map)
        val (second in-word-map)]
    {:word (trim cay)
     :freq val
     :sentence-indexes (get-word-indexes cay in-sentences)}))

;;gets the frequency of all words and creates a map containing the word breakdown
(defn concordance [in-str]
  (map #(create-word-map % (get-sentences in-str)) (frequencies (get-words in-str))))

;;program entry point
(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (doseq [item (sort-by :word (concordance (first args)))]
    (println item)))

(comment
  (def test-string "Given an arbitrary text document written in English, write a program that will generate a
  concordance, i.e. an alphabetical list of all word occurrences, labeled with word frequencies.
  Bonus: label each word with the sentence numbers in which each occurrence appeared."))
